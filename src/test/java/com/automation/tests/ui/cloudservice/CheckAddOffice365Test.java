package com.automation.tests.ui.cloudservice;

import com.automation.businessobjects.model.enumeration.CloudDevice;
import com.automation.tests.ui.base.BaseTest;
import com.automation_ui.pages.HomePage;
import com.automation_ui.pages.LoginPage;
import com.automation_ui.pages.popup.Office365AdminPopUp;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class CheckAddOffice365Test extends BaseTest {
    private LoginPage loginPage = new LoginPage();
    private Office365AdminPopUp office365AdminPopUp = new Office365AdminPopUp();
    private HomePage homePage = new HomePage();

    @Test(description = "1. Navigate to sign in page https://***" +
            "2. Login" +
            "3. Click 'Add' under 'Cloud Services'" +
            "4. Make sure that it is possible to create a Office 365 Admin")
    public void verifyAbilityAddOffice365() {

        loginPage.login()
                .clickAddCloudService();
        assertThat(new HomePage().isPresentCloud(CloudDevice.OFFICE_365_ADMIN))
                .as("'Office 365 Admin' isn't present")
                .isTrue();
        homePage.selectCloud(CloudDevice.OFFICE_365_ADMIN);
        office365AdminPopUp
                .setNameClickNext("5432")
                .clickNext();
        assertThat(new HomePage().isPresentHeader())
                .as("Home page still present")
                .isFalse();
    }
}
