package com.automation_ui.pages.popup;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$x;

public class Office365AdminPopUp {

    private SelenideElement nameInput = $x("//input[@id='name']");
    private SelenideElement nextBtn = $x("//a[@id='next-button-1']");

    public Office365AdminPopUp setNameClickNext(String name) {
        nameInput.val(name);
        return this;
    }
    public void clickNext() {
        nextBtn.click();
        nextBtn.shouldNot(exist);
    }
}
