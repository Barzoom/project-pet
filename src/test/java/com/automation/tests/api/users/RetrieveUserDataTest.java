package com.automation.tests.api.users;

import com.automation.businessobjects.model.UserEntity;
import com.automation.steps.api.UsersAPIControllerSteps;
import org.testng.annotations.Test;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;


public class RetrieveUserDataTest {

    private UsersAPIControllerSteps usersAPIControllerSteps = new UsersAPIControllerSteps(); // create controller for make get request to User resource
    public static final String USER_ID = "zhc4v6-5ev7di-9hhhlm";

    @Test(description = "1. Login to Project as admin" +
            "2. Retrieve user data via UserId" +
            "3. Verify UserEntity")
    public void verifyUserData() {
        UserEntity userEntity = usersAPIControllerSteps.getUser(USER_ID);
        assertThat(userEntity).hasNoNullFieldsOrPropertiesExcept("deletionDeadline", "external_id"); //verify required fields for user
    }
}
