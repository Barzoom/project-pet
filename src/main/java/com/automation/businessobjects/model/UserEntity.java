package com.automation.businessobjects.model;

import com.google.gson.annotations.SerializedName;
import lombok.*;

import java.time.OffsetDateTime;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString

public class UserEntity {
    private Boolean enabled;

    private OffsetDateTime created;
    private String product;
    private String parent;
    @SerializedName("deletion-deadline")
    private OffsetDateTime deletionDeadline;
    private Boolean subscribed;
    private String external_id;
}

