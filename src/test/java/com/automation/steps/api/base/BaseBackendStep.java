package com.automation.steps.api.base;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.automation.utils.properties.APIProperties;
import com.automation.utils.properties.UIProperties;
import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.SneakyThrows;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

import static io.restassured.RestAssured.DEFAULT_URI;
import static io.restassured.RestAssured.basic;
import static org.assertj.core.api.Assertions.assertThat;

public abstract class BaseBackendStep {

    private static final Logger LOG = LogManager.getLogger(BaseBackendStep.class.getName());

    public BaseBackendStep() { // config RestAssured with user name and password for authentication in system
        if (RestAssured.baseURI.equals(DEFAULT_URI)) {
            RestAssured.baseURI = APIProperties.getBaseUrl();
            //add log
            RestAssured.defaultParser = Parser.XML;
            RestAssured.authentication = basic(UIProperties.getUserBaseName(), UIProperties.getUserBasePassword());
        }
    }

    protected Response makeGETRequest(String urlPart, Map<String, Object> headers) {
        RequestSpecification request = RestAssured.given().relaxedHTTPSValidation();
        request.headers(headers);
        LOG.info("Request Get for URL: " + urlPart);
        LOG.info("Request Headers: " + headers.toString());
        Response response = request.get(urlPart);
        LOG.info("Response Time in milliseconds:" + response.getTime());
        return response;
    }

    protected void checkStatusCodeIs(Response response, int code) {
        assertThat(response.getStatusCode())
                .as("Incorrect status code. Response:" + response.getBody().asString())
                .isEqualTo(code);

    }

    @SneakyThrows
    protected <T> T getEntityModelFromResponse(Response response, Class<T> t) {
        XmlMapper mapper = new XmlMapper(); // create XmlMapper for mapping out response from system
        mapper.findAndRegisterModules(); //connect all Jackson modules
        return response.getBody().asString().length() != 0 ? mapper.readValue(response.asString(), t) : null; // mapping response to our POJO
    }

}


