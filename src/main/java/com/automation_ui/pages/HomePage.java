package com.automation_ui.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.automation.businessobjects.model.enumeration.CloudDevice;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$x;

public class HomePage {

    SelenideElement header = $x("//div[@class='content devices']");
    SelenideElement addBtn = $x(".//a[@id='cloud-devices-button']");

    private ElementsCollection getCloudsName() {
        return addBtn.$$x("./..//span[contains(@class,'device')]");
    }

    public HomePage clickAddCloudService() {
        addBtn.should(exist).shouldBe(visible).click();
        return this;
    }

    public void selectCloud(CloudDevice cloudDevice) {
        getCloudsName().findBy(textCaseSensitive(cloudDevice.name)).click();
    }

    public boolean isPresentCloud(CloudDevice cloudDevice) {
        return getCloudsName().findBy(textCaseSensitive(cloudDevice.name)).isDisplayed();
    }

    public boolean isPresentHeader() {
        return header.isDisplayed();
    }

}
