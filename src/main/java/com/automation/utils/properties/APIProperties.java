package com.automation.utils.properties;

import java.util.ResourceBundle;
public class APIProperties { //config for API properties

    private static final String AUTOMATION_PROPERTIES_API = "automation_api";
    private static final String BASE_API_URL = "base.url";
    private static final String GET_USERS_URL_PART = "getUsers.url.part";

    private static ResourceBundle resourceBundle = ResourceBundle.getBundle(AUTOMATION_PROPERTIES_API);

    private APIProperties() {
    }

    public static String getBaseUrl() {
        return System.getProperty(BASE_API_URL, resourceBundle.getString(BASE_API_URL));
    }

    public static String getUsersUrlPart(String userId) {
        return String.format(resourceBundle.getString(GET_USERS_URL_PART), userId);
    }
}
