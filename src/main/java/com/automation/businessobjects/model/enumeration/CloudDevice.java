package com.automation.businessobjects.model.enumeration;

public enum CloudDevice {
    SALESFORCE("Salesforce"),
    G_SUIT("G Suit"),
    OFFICE_365_BUSINESS("Office 365 Business"),
    OFFICE_365_HOME("Office 365 Home"),
    OFFICE_365_ADMIN("Office 365 Admin"),
    FTP_CONNECTOR("FTP connector"),
    GOOGLE_DRIVE("Google Drive"),
    IMAP_CONNECTOR("IMAP Connector"),
    EXCHANGE_MAIL_CONNECTOR("Exchange Mail Connector"),
    GOOGLE_MAIL_CONNECTOR("Google Mail Connector"),
    DYNAMICS_365("Dynamics 365");

    public String name;

    CloudDevice(String name){
        this.name = name;
    }
}
