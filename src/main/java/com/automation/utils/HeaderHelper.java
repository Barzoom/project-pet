package com.automation.utils;

import java.util.HashMap;
import java.util.Map;

import static org.apache.http.entity.ContentType.APPLICATION_XML;

public class HeaderHelper {

    public static Map<String, Object> getDefaultHeaders(){
        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put("Content-Type", APPLICATION_XML.getMimeType());
        return headers;
    }
}
