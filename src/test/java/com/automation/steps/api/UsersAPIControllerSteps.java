package com.automation.steps.api;

import com.automation.businessobjects.model.UserEntity;
import com.automation.steps.api.base.BaseBackendStep;
import com.automation.utils.HeaderHelper;
import com.automation.utils.properties.APIProperties;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UsersAPIControllerSteps extends BaseBackendStep {

    private static final Logger LOG = LogManager.getLogger(UsersAPIControllerSteps.class.getName()); //TODO add Log4j config

    private Response getUserResponse(String userId) {
        return makeGETRequest(APIProperties.getUsersUrlPart(userId),
                HeaderHelper.getDefaultHeaders());
    }

    public UserEntity getUser(String userId) {
        return getUser(userId, UserEntity.class, 200);
    }

    private <T> T getUser(String userId, Class<T> t, int statusCode) {
        Response response = getUserResponse(userId);
        checkStatusCodeIs(response, statusCode);
        LOG.info(response.getBody().asString());
        return getEntityModelFromResponse(response, t);
    }
}
