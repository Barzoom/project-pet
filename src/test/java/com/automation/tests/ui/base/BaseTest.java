package com.automation.tests.ui.base;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.automation.utils.properties.UIProperties;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import static com.codeborne.selenide.Selenide.open;

public class BaseTest {

    @BeforeClass(alwaysRun = true)
    public void openProduct() {
        Configuration.timeout = 10000;
        Configuration.startMaximized = true;
        open(UIProperties.getBaseUrl());
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        WebDriverRunner.getWebDriver().close();
    }
}
