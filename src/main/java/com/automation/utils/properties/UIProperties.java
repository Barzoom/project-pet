package com.automation.utils.properties;

import java.util.ResourceBundle;

import static org.assertj.core.api.Assertions.assertThat;

public class UIProperties { //config for UI properties

    private static final String PROPERTIES_UI = "automation_ui";
    private static final String BASE_UI_URL = "selenide.base.ui.url";
    private static final String USER_BASE_PASSWORD = "user.base.password";
    private static final String USER_BASE_NAME = "user.base.name";

    private static ResourceBundle resourceBundle = ResourceBundle.getBundle(PROPERTIES_UI);

    private UIProperties() {
    }

    public static String getBaseUrl() {
        return System.getProperty(BASE_UI_URL, resourceBundle.getString(BASE_UI_URL));
    }

    public static String getUserBasePassword() {
        String userBasePassword = resourceBundle.getString(USER_BASE_PASSWORD);
        assertThat(userBasePassword)
                .as("User base password is empty")
                .hasSizeGreaterThanOrEqualTo(1);
        return userBasePassword;
    }

    public static String getUserBaseName() {
        String userBaseName = resourceBundle.getString(USER_BASE_NAME);
        assertThat(userBaseName)
                .as("User base name is empty")
                .hasSizeGreaterThanOrEqualTo(1);
        return userBaseName;
    }
}
