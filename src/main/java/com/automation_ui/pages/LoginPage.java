package com.automation_ui.pages;

import com.codeborne.selenide.SelenideElement;
import com.automation.utils.properties.UIProperties;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class LoginPage {

    private SelenideElement emailField = $x("//input[@name='login']");
    private SelenideElement passwordField = $x("//input[@name='password']");
    private SelenideElement signInBtn = $x("//input[@id='login-form-login-button']");

    public HomePage login() {
        emailField.val(UIProperties.getUserBaseName());
        passwordField.val(UIProperties.getUserBasePassword());
        signInBtn.should(exist).shouldBe(visible).click();
        return new HomePage();
    }

}
